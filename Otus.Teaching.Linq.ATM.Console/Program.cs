﻿using System;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;

namespace Otus.Teaching.Linq.ATM.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Старт приложения-банкомата...");

            var atmManager = CreateATMManager();

            var user = atmManager.GetUserByIndex(new Random().Next(0, atmManager.GetUsersCount()));
            if (user is null) 
            { 
                System.Console.WriteLine("Клиент не найден!");
                System.Console.ReadLine();
                return;
            }

            var check = atmManager.GetUser(user.Login, user.Password);
            System.Console.WriteLine($"\n[1] Клиент {check} имеет логин - {check.Login}, пароль - [{check.Password}].");

            var userAccounts = atmManager.GetUserAccounts(user);
            System.Console.WriteLine($"\n[2] Список счетов клиента:\nКлиент: {user}\n{string.Join("\n",userAccounts)}");

            var operationsHistory = atmManager.GetAccountsOperationsHistory(user);
            var message = operationsHistory.Any() ? $"{string.Join(";", operationsHistory)}." : $"Не совершал операции.";
            System.Console.WriteLine($"\n[3] Список операций клиента:\nКлиент: {user}{message}");

            const decimal sum = 1000;
            var u = atmManager.GetUsersSumGreaterN(sum);
            System.Console.WriteLine($"\n[4] Клиенты имеющие на счете более {sum:f2} рублей:\n{string.Join("\n", u)}");

            var o = atmManager.GetOperationsHistory(Core.Entities.OperationType.InputCash);
            System.Console.WriteLine($"\n[5] Вывод данных о всех операциях пополнения счёта с указанием владельца каждого счёта:\n{string.Join("\n", o)}");

            System.Console.WriteLine("\n\nЗавершение работы приложения-банкомата...");
            System.Console.ReadLine();
        }

        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();
                
            return new ATMManager(accounts, users, history);
        }
    }
}