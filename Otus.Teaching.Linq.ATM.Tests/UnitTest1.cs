using NUnit.Framework;
using Otus.Teaching.Linq.ATM.Core.Entities;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;
using System.Linq;

namespace Otus.Teaching.Linq.ATM.Tests
{
    public class Tests
    {
        private ATMManager atmManager;
        [SetUp]
        public void Setup()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();
            
            atmManager = new ATMManager(accounts, users, history);
        }

        [Test]
        public void Test1()
        {
            Assert.Pass();
        }
        
        [TestCase("cant", "333")]
        [TestCase("star", "444")]
        [TestCase(default(string), "PASSWORD")]
        [TestCase("LOGIN", default(string))]
        [TestCase(default(string), default(string))]
        public void GetUser_Test(string login, string password)
        {
            var user = atmManager.GetUser(login, password);
            if (string.IsNullOrEmpty(login) || string.IsNullOrEmpty(password))
            {
                Assert.Null(user);
            }
            else
            {
                Assert.IsNotNull(user);
            }
        }
        [Test]
        public void GetUserAccount_Test()
        {
            var user = atmManager.GetUser("snow", "111");
            var userAccounts = atmManager.GetUserAccounts(user);
            Assert.IsNotNull(userAccounts);
        }

        [Test]
        public void GetAccountsOperationsHistory_Test()
        {
            var user = atmManager.GetUser("snow", "111");
            var accountsOperationsHistory = atmManager.GetAccountsOperationsHistory(user);
            Assert.IsNotNull(accountsOperationsHistory);
        }

        [Test]
        public void GetUsersSumGreaterN_Test()
        {
            var users = atmManager.GetUsersSumGreaterN(1000);
            Assert.IsNotNull(users);
            Assert.AreEqual(users.Count, 5);
        }
        [Test]
        public void GetOperationsHistory_Test()
        {
            var operations = atmManager.GetOperationsHistory(OperationType.InputCash);
            Assert.IsNotNull(operations);
            Assert.IsFalse(operations.Any(a => a.OperationsHistory.OperationType == OperationType.OutputCash));
            Assert.IsTrue(operations.Any(a => a.OperationsHistory.OperationType == OperationType.InputCash));
        }
    }
}