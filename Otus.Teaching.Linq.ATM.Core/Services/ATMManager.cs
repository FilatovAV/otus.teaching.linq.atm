﻿using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.DTO;
using Otus.Teaching.Linq.ATM.Core.Entities;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }
        
        public IEnumerable<User> Users { get; private set; }
        
        public IEnumerable<OperationsHistory> History { get; private set; }
        
        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }
        /// <summary> Клиенты с суммой на счете больше заданной. </summary>
        public List<User> GetUsersSumGreaterN(decimal sum)
        {
            //Для начала получаем аккаунты содержащие сумму больше запрашиваемой.
            //Для того чтобы избежать лишних итераций сразу преобразуем ее в массив данных.
            //Затем берем всех пользователей которые хоть раз встречаются в коллекции rAccounts.
            var rAccounts = Accounts.Where(w => w.CashAll > sum).ToArray();
            return Users.Where(w => rAccounts.Any(a => a.UserId == w.Id)).ToList();
        }
        /// <summary> 4. Вывод данных о всех операциях пополнения счёта с указанием владельца каждого счёта </summary>
        public List<History> GetOperationsHistory(OperationType operationType)
        {
            return History
                .Where(w=> w.OperationType == operationType)
                .Join(
                Accounts, 
                h => h.AccountId, 
                a => a.Id,
                (h, a) => new History 
                    {OperationsHistory = h, User = Users.FirstOrDefault(f => f.Id == a.UserId) }).ToList();
        }
        /// <summary> 3. Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту </summary>
        public List<History> GetAccountsOperationsHistory(User user)
        {
            var userAccounts = GetUserAccounts(user).ToArray();

            return userAccounts.Join(
                History,
                ua => ua.Id,
                h => h.AccountId,
                (ua, h) => new History
                    { Account = ua, OperationsHistory = h }).ToList();
        }
        /// <summary> Все счета клиента. </summary>
        public List<Account> GetUserAccounts(User user)
        {
            return Accounts.Where(f => f.UserId == user.Id).ToList();
        }
        /// <summary> Получить клиента по Id. </summary>
        public User GetUserByIndex(int id)
        {
            return Users.FirstOrDefault(f => f.Id == id);
        }
        /// <summary> Получить клиента по логину и паролю. </summary>
        public User GetUser(string login, string password)
        {
            return Users.FirstOrDefault(f => f.Login == login && f.Password == password);
        }
        /// <summary> Число клиентов. </summary>
        public int GetUsersCount()
        {
            return Users.Count();
        }
    }
}