﻿using Otus.Teaching.Linq.ATM.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Linq.ATM.Core.DTO
{
    public class History
    {
        public Account Account { get; set; }
        public OperationsHistory OperationsHistory { get; set; }
        public User User { get; set; }
        public override string ToString()
        {
            return $"{User}\n{OperationsHistory}";
        }
    }
}
